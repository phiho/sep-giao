//View the status of the event scheduler thread:
    SHOW PROCESSLIST;
//If the event scheduler is not enabled, you can set the event_scheduler system variable to enable and start it:
    SET GLOBAL event_scheduler = ON;
//Creating new MySQL events:
    CREATE EVENT IF NOT EXISTS test_event_01
    ON SCHEDULE AT CURRENT_TIMESTAMP
    DO
    INSERT INTO messages(message,created_at)
    VALUES('Test MySQL Event 1',NOW());
//Events không cho drop:
    CREATE EVENT test_event_02
    ON SCHEDULE AT CURRENT_TIMESTAMP + INTERVAL 1 MINUTE
    ON COMPLETION PRESERVE
    DO
    INSERT INTO messages(message,created_at)
    VALUES('Test MySQL Event 2',NOW());
//Creating a recurring event example:
    CREATE EVENT test_event_03
    ON SCHEDULE EVERY 1 MINUTE
    STARTS CURRENT_TIMESTAMP
    ENDS CURRENT_TIMESTAMP + INTERVAL 1 HOUR
    DO
    INSERT INTO messages(message,created_at)
    VALUES('Test MySQL recurring Event',NOW());
//show all exists events from a database_name:
    SHOW EVENTS FROM database_name;